% reportae
% By Almut Eisentraeger < http://www.almutei.de >

%-------------------------- identification ---------------------
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{reportae}[2014]
%-------------------------- end identification ---------------------

% ----------- Declarations of Options ----------------------------
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{scrartcl}}
\ProcessOptions\relax
\LoadClass[a4paper,10pt,headsepline]{scrartcl}


%%%%% Packages and layout %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\RequirePackage[utf8]{inputenc}
\RequirePackage[UKenglish]{babel}

\RequirePackage[hmargin=2cm,vmargin=1cm,includehead]{geometry}
\RequirePackage[compact]{titlesec} 
\RequirePackage{paralist}

\RequirePackage{fancyhdr}
% \RequirePackage{calc}
\RequirePackage{ifthen}
\RequirePackage[nodayofweek]{datetime}
\newdateformat{yyyymmdd}{\THEYEAR-\twodigit{\THEMONTH}-\twodigit{\THEDAY}}
\setlength\parindent{0in}
\setlength\parskip{0.1in}

\RequirePackage{amsmath,amsthm,amssymb,mathpazo,listings,mathtools}
\allowdisplaybreaks

\RequirePackage[round]{natbib}


\RequirePackage{pstricks,setspace,graphicx}
\graphicspath{{./},{./img/}}
\renewcaptionname{UKenglish}{\figurename}{Fig.} 
\RequirePackage{flafter}
\RequirePackage{tikz,pgfplots}
\def\fps@figure{htbp}


\RequirePackage{hyperref}
\RequirePackage{lastpage}


%%%% Author and title %%%%%%%%%%%%%%%%%%%
\title{}
\author{}
\renewcommand{\maketitle}{{\flushleft{\huge{\bfseries{\@title}}}\par}}
\newcommand{\@shorttitle}{\@title}
\renewcommand{\title}[2][]{%
  \renewcommand{\@title}{#2}%
  \ifx&#1&% optional argument empty
%     \newlength{\headtitlelength}
%     \setlength{\headtitlelength}{\textwidth}
%     \setlength{\headtitlelength}{\textwidth%
%       -\widthof{\@author: }%
%       -\widthof{\yyyymmdd\today{} \currenttime}%
%       -\widthof{10/10}
%       -3cm
%     }
%     \renewcommand{\@shorttitle}{\truncate{\headtitlelength}{\@title}}
    \renewcommand{\@shorttitle}{#2}%
  \else % optional argument empty
    \renewcommand{\@shorttitle}{#1}%
  \fi%
}


%%%%%%%%%%% HEADER / FOOTER %%%%%%%%%%%

\pagestyle{fancy}
\fancyhead{}\fancyfoot{}
\lhead{\ifthenelse{\equal{\@author}{}}{\relax}{\@author: }\@shorttitle}
\rhead{{\yyyymmdd\today{} \currenttime}\hspace{1em}\thepage/\pageref{LastPage}}

%%%%%%%%%%% AUTOREF %%%%%%%%%%%%%%%%%%%
\renewcommand*{\figureautorefname}{Fig.}
\renewcommand*\sectionautorefname{\S\!}
\renewcommand*\subsectionautorefname{\S\!}
\renewcommand*\subsubsectionautorefname{\S\!}
\renewcommand*\equationautorefname{equ.}


%%%%%%%%%%% FLOAT PLACEMENT %%%%%%%%%%%%
% From http://mintaka.sdsu.edu/GF/bibliog/latex/floats.html 
% Alter some LaTeX defaults for better treatment of figures:
    % See p.105 of "TeX Unbound" for suggested values.
    % See pp. 199-200 of Lamport's "LaTeX" book for details.
    %   General parameters, for ALL pages:
    \renewcommand{\topfraction}{0.9}	% max fraction of floats at top
    \renewcommand{\bottomfraction}{0.8}	% max fraction of floats at bottom
    %   Parameters for TEXT pages (not float pages):
    \setcounter{topnumber}{2}
    \setcounter{bottomnumber}{2}
    \setcounter{totalnumber}{4}     % 2 may work better
    \setcounter{dbltopnumber}{2}    % for 2-column pages
    \renewcommand{\dbltopfraction}{0.9}	% fit big float above 2-col. text
    \renewcommand{\textfraction}{0.07}	% allow minimal text w. figs
    %   Parameters for FLOAT pages (not text pages):
    \renewcommand{\floatpagefraction}{0.7}	% require fuller float pages
	% N.B.: floatpagefraction MUST be less than topfraction !!
    \renewcommand{\dblfloatpagefraction}{0.7}	% require fuller float pages




%%%%%%%%%%% COMMENTING %%%%%%%%%%%%%%%%%
\newcommand{\comment}[2][!]{\marginpar{\begin{minipage}{1cm}\hfill\color{blue}\ttfamily {#1}\end{minipage}}{\color{blue}\ttfamily [#2]}}
\newcommand{\todo}[2][]{\marginpar{\begin{minipage}{1cm}\hfill\color{red}\ttfamily {TODO}\end{minipage}}{\color{red}\ttfamily [#2]}}

%%%% Lazy macros %%%%%%%%%%%%%%%%%%%%%%%%%%%
\newcommand{\eg}{\emph{e.\,g.}}
\newcommand{\ie}{\emph{i.\,e.}}
\newcommand{\cf}{\emph{cf.}}
\newcommand{\bigO}[1]{\ensuremath{\mathcal{O}\!\left(#1\right)}}
\DeclareMathOperator{\sgn}{sgn}